package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"

	pokitdok "gitlab.com/seedifferently/pokitdok-go"
)

type copay struct {
	Copayment struct {
		Amount   string
		Currency string
	}
	CoverageLevel string
	InPlanNetwork string
	Messages      []struct {
		Message string
	}
	PlanDescription  string
	ServiceTypeCodes []string
	ServiceTypes     []string
}

type deductible struct {
	BenefitAmount struct {
		Amount   string
		Currency string
	}
	CoverageLevel   string
	EligibilityDate string
	InPlanNetwork   string
	Messages        []struct {
		Message string
	}
	PlanDescription  string
	ServiceTypeCodes []string
	ServiceTypes     []string
	TimePeriod       string
}

var (
	clientID     string
	clientSecret string
	serviceType  string
)

func init() {
	flag.StringVar(&clientID, "client-id", "", "Your API client ID. (Can also be specified via a POKITDOK_CLIENT_ID environment variable.)")
	flag.StringVar(&clientSecret, "client-secret", "", "Your API client Secret. (Can also be specified via a POKITDOK_CLIENT_SECRET environment variable.)")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "%s -client-id POKITDOK_CLIENT_ID -client-secret POKITDOK_CLIENT_SECRET\n\n", os.Args[0])
		fmt.Fprint(os.Stderr, "Options:\n\n")
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()

	// Check that client id and secret are specified (either via flag or env)
	if clientID == "" {
		clientID = os.Getenv("POKITDOK_CLIENT_ID")
	}
	if clientSecret == "" {
		clientSecret = os.Getenv("POKITDOK_CLIENT_SECRET")
	}

	if clientID == "" || clientSecret == "" {
		flag.Usage()
		os.Exit(1)
	}

	client := pokitdok.NewAPIClient(clientID, clientSecret)
	opt := &pokitdok.EligibilityOptions{}
	reader := bufio.NewReader(os.Stdin)

StartOver:
	// Welcome/intro text
	fmt.Println("Welcome to the eligibility checker CLI tool.")
	fmt.Println("You will be asked to provide input for a few fields, then an eligibility check will be performed.")
	fmt.Print("Press Control-C at any time to abort.\n\n")

	// Input fields
	fmt.Print("Birth Date (YYYY-MM-DD): ")
	birthDate, _ := reader.ReadString('\n')
	opt.Member.BirthDate = strings.TrimSpace(birthDate)
	fmt.Print("First Name: ")
	firstName, _ := reader.ReadString('\n')
	opt.Member.FirstName = strings.TrimSpace(firstName)
	fmt.Print("Last Name: ")
	lastName, _ := reader.ReadString('\n')
	opt.Member.LastName = strings.TrimSpace(lastName)
	fmt.Print("Trading Partner ID: ")
	tradingPartnerID, _ := reader.ReadString('\n')
	opt.TradingPartnerID = strings.TrimSpace(tradingPartnerID)
	fmt.Print("Service Type (default: health_benefit_plan_coverage): ")
	serviceType, _ := reader.ReadString('\n')
	serviceType = strings.TrimSpace(serviceType)
	if serviceType == "" {
		serviceType = "health_benefit_plan_coverage"
	}
	opt.ServiceTypes = []string{serviceType}

	// Check that the inputs were filled out
	if opt.Member.BirthDate == "" || opt.Member.FirstName == "" || opt.Member.LastName == "" || opt.TradingPartnerID == "" {
		fmt.Print("\nSorry, all fields are required. Starting over...\n*\n*\n*\n")
		goto StartOver
	}

	fmt.Print("\nGot it! Please wait while the request is processed...\n\n")

	// Perform the request to the Eligibility service
	e, err := client.Eligibility.Do(opt)
	if err != nil {
		fmt.Println("OOPS! It looks like an error was encountered:")
		fmt.Println(err.Error())
		fmt.Print("\nStarting over...\n*\n*\n*\n")
		goto StartOver
	}

	// Check if the request was rejected
	if !e.Data.ValidRequest || e.Data.RejectReason != "" {
		fmt.Println("OOPS! It looks like the request was rejected:")

		if e.Data.RejectReason != "" {
			fmt.Printf("Reject Reason: %s\n", e.Data.RejectReason)
		}
		if e.Data.FollowUpAction != "" {
			fmt.Printf("Follow Up Action: %s\n", e.Data.FollowUpAction)
		}

		fmt.Print("\nStarting over...\n*\n*\n*\n")
		goto StartOver
	}

	// Warn if coverage is not active
	if !e.Data.Coverage.Active {
		fmt.Print("WARNING: Coverage is not active.\n\n")
	}

	// From what I can tell, if the service type isn't listed in the coverage
	// related object, it isn't covered/won't have any copays or deductibles.
	if !isStringInSlice(serviceType, e.Data.Coverage.ServiceTypes) {
		fmt.Printf("No eligibility found for: %s\n\n", serviceType)
	} else {
		// Print out any copay info for the service type
		copays := copaysForServiceType(e, serviceType)
		fmt.Printf("[ Copays for %s ]\n\n", serviceType)
		if len(copays) == 0 {
			fmt.Print("None found.\n\n")
		} else {
			for k, v := range copays {
				a := []string{v.Copayment.Amount, v.Copayment.Currency}
				fmt.Printf("#%d.\tAmount: %s\n", k+1, strings.Join(a, ""))
				fmt.Printf("\tCoverage Level: %s\n", v.CoverageLevel)
				fmt.Printf("\tIn Plan Network: %s\n", v.InPlanNetwork)
				fmt.Printf("\tPlan Description: %s\n", v.PlanDescription)
				if len(v.Messages) > 0 {
					fmt.Print("\tMessages:\n")
					for _, m := range v.Messages {
						fmt.Printf("\t\t%s\n", m.Message)
					}
				}
				fmt.Print("\n\n")
			}
		}

		// Print out any deductible info for the service type
		deductibles := deductiblesForServiceType(e, serviceType)
		fmt.Printf("[ Deductibles for %s ]\n\n", serviceType)
		if len(deductibles) == 0 {
			fmt.Print("None found.\n\n")
		} else {
			for k, v := range deductibles {
				a := []string{v.BenefitAmount.Amount, v.BenefitAmount.Currency}
				fmt.Printf("#%d.\tAmount: %s\n", k+1, strings.Join(a, ""))
				fmt.Printf("\tCoverage Level: %s\n", v.CoverageLevel)
				fmt.Printf("\tIn Plan Network: %s\n", v.InPlanNetwork)
				fmt.Printf("\tPlan Description: %s\n", v.PlanDescription)
				fmt.Printf("\tEligibility Date: %s\n", v.EligibilityDate)
				fmt.Printf("\tTime Period: %s\n", v.TimePeriod)
				if len(v.Messages) > 0 {
					fmt.Print("\tMessages:\n")
					for _, m := range v.Messages {
						fmt.Printf("\t\t%s\n", m.Message)
					}
				}
				fmt.Print("\n\n")
			}
		}
	}

	// Ask what to do next
	fmt.Print("Would you like to start over and run another check? [Y/n] ")
	startOver, _ := reader.ReadString('\n')
	startOver = strings.ToLower(strings.TrimSpace(startOver))
	if startOver == "" || startOver == "y" || startOver == "yes" {
		fmt.Print("*\n*\n*\n")
		goto StartOver
	} else {
		fmt.Print("\nThanks, goodbye!\n")
	}
}

func copaysForServiceType(e *pokitdok.Eligibility, s string) []copay {
	var copays []copay

	for _, v := range e.Data.Coverage.Copay {
		if isStringInSlice(s, v.ServiceTypes) {
			copays = append(copays, copay(v))
		}
	}

	return copays
}

func deductiblesForServiceType(e *pokitdok.Eligibility, s string) []deductible {
	var deductibles []deductible

	for _, v := range e.Data.Coverage.Deductibles {
		if isStringInSlice(s, v.ServiceTypes) {
			deductibles = append(deductibles, deductible(v))
		}
	}

	return deductibles
}

func isStringInSlice(n string, h []string) bool {
	for _, v := range h {
		if v == n {
			return true
		}
	}

	return false
}
