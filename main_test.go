package main

import (
	"testing"

	pokitdok "gitlab.com/seedifferently/pokitdok-go"
)

func Test_copaysForServiceType(t *testing.T) {
	copays := copaysForServiceType(eligibility, "professional_physician_visit_office")
	if len(copays) != 2 {
		t.Errorf("len(copays) = %+v, want %+v", len(copays), 2)
	}
}

func Test_deductiblesForServiceType(t *testing.T) {
	deductibles := deductiblesForServiceType(eligibility, "health_benefit_plan_coverage")

	if len(deductibles) != 8 {
		t.Errorf("len(deductibles) = %+v, want %+v", len(deductibles), 8)
	}
}

func Test_isStringInSlice(t *testing.T) {
	check := isStringInSlice("health_benefit_plan_coverage", eligibility.Data.Coverage.ServiceTypes)
	testEqual(t, "isStringInSlice(\"health_benefit_plan_coverage\", e) = %+v, want %+v", check, true)
	check = isStringInSlice("reincarnation", eligibility.Data.Coverage.ServiceTypes)
	testEqual(t, "isStringInSlice(\"reincarnation\", e) = %+v, want %+v", check, false)
}

func testEqual(t *testing.T, msg string, args ...interface{}) bool {
	if args[len(args)-2] != args[len(args)-1] {
		t.Errorf(msg, args...)
		return false
	}
	return true
}

var eligibility = &pokitdok.Eligibility{Data: struct {
	ValidRequest   bool   "json:\"valid_request\""
	RejectReason   string "json:\"reject_reason\""
	FollowUpAction string "json:\"follow_up_action\""
	Coverage       struct {
		Active           bool     "json:\"active\""
		ServiceTypes     []string "json:\"service_types\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		Coinsurance      []struct {
			BenefitPercent   float64  "json:\"benefit_percent\""
			CoverageLevel    string   "json:\"coverage_level\""
			InPlanNetwork    string   "json:\"in_plan_network\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
		} "json:\"coinsurance\""
		Copay []struct {
			Copayment struct {
				Amount   string "json:\"amount\""
				Currency string "json:\"currency\""
			} "json:\"copayment\""
			CoverageLevel string "json:\"coverage_level\""
			InPlanNetwork string "json:\"in_plan_network\""
			Messages      []struct {
				Message string "json:\"message\""
			} "json:\"messages\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
		} "json:\"copay\""
		Deductibles []struct {
			BenefitAmount struct {
				Amount   string "json:\"amount\""
				Currency string "json:\"currency\""
			} "json:\"benefit_amount\""
			CoverageLevel   string "json:\"coverage_level\""
			EligibilityDate string "json:\"eligibility_date\""
			InPlanNetwork   string "json:\"in_plan_network\""
			Messages        []struct {
				Message string "json:\"message\""
			} "json:\"messages\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
			TimePeriod       string   "json:\"time_period\""
		} "json:\"deductibles\""
	} "json:\"coverage\""
	ServiceTypes     []string    "json:\"service_types\""
	ServiceTypeCodes []string    "json:\"service_type_codes\""
	Errors           interface{} "json:\"errors\""
}{ValidRequest: true, RejectReason: "", FollowUpAction: "", Coverage: struct {
	Active           bool     "json:\"active\""
	ServiceTypes     []string "json:\"service_types\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	Coinsurance      []struct {
		BenefitPercent   float64  "json:\"benefit_percent\""
		CoverageLevel    string   "json:\"coverage_level\""
		InPlanNetwork    string   "json:\"in_plan_network\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
	} "json:\"coinsurance\""
	Copay []struct {
		Copayment struct {
			Amount   string "json:\"amount\""
			Currency string "json:\"currency\""
		} "json:\"copayment\""
		CoverageLevel string "json:\"coverage_level\""
		InPlanNetwork string "json:\"in_plan_network\""
		Messages      []struct {
			Message string "json:\"message\""
		} "json:\"messages\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
	} "json:\"copay\""
	Deductibles []struct {
		BenefitAmount struct {
			Amount   string "json:\"amount\""
			Currency string "json:\"currency\""
		} "json:\"benefit_amount\""
		CoverageLevel   string "json:\"coverage_level\""
		EligibilityDate string "json:\"eligibility_date\""
		InPlanNetwork   string "json:\"in_plan_network\""
		Messages        []struct {
			Message string "json:\"message\""
		} "json:\"messages\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
		TimePeriod       string   "json:\"time_period\""
	} "json:\"deductibles\""
}{Active: true, ServiceTypes: []string{"health_benefit_plan_coverage", "professional_physician_visit_office"}, ServiceTypeCodes: []string{"30", "98"}, Coinsurance: []struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{BenefitPercent: 0, CoverageLevel: "employee_and_spouse", InPlanNetwork: "yes", PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}, struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{BenefitPercent: 0.5, CoverageLevel: "employee_and_spouse", InPlanNetwork: "no", PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}}, Copay: []struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{Copayment: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "20", Currency: "USD"}, CoverageLevel: "employee_and_spouse", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "PRIMARY OFFICE"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}, struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{Copayment: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "30", Currency: "USD"}, CoverageLevel: "employee_and_spouse", InPlanNetwork: "not_applicable", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "GYN OFFICE VS"}, struct {
	Message string "json:\"message\""
}{Message: "GYN VISIT"}, struct {
	Message string "json:\"message\""
}{Message: "SPEC OFFICE"}, struct {
	Message string "json:\"message\""
}{Message: "SPEC VISIT"}, struct {
	Message string "json:\"message\""
}{Message: "PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}}, Deductibles: []struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "6000", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "2013-01-01", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "5956.09", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "3000", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "2013-01-01", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "2983.57", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "12000", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "2013-01-01", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "11956.09", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "6000", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "2013-01-01", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "5983.57", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}}}, ServiceTypes: []string{"health_benefit_plan_coverage", "professional_physician_visit_office"}, ServiceTypeCodes: []string{"30", "98"}, Errors: interface{}(nil)}}
