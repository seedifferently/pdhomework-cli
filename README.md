Eligibility Check CLI
=====================


What?
-----

This is a simple CLI tool for running an eligibility check against the PokitDok
Eligibility API service, and printing the returned copay/deductible information.


How?
----

Simply download and run a binary release for your platform from the
[tags](https://gitlab.com/seedifferently/pdhomework-cli/tags) section.


Or, if you already have Go v1.8 or later installed:

1. Run: `go get gitlab.com/seedifferently/pdhomework-cli`

2. Run: `$GOPATH/pdhomework-cli` (where $GOPATH is your environment's GOPATH)


Alternatively, you may build from source:

1. Clone the repository.

2. Install [Go](https://golang.org/doc/install) (requires v1.8 or later).

3. Install the PokitDok Go library: `go get gitlab.com/seedifferently/pokitdok-go`

4. Build the project: `go build`

5. Run: `./pdhomework-cli`
